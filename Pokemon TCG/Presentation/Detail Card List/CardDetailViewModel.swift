//
//  DetailCardViewModel.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 08/06/23.
//

import Foundation
import RxSwift
import RxCocoa

extension CardDetailViewModel {
    struct Input {
        var fetchDetailCard: Driver<String>
        var fetchPagination: Driver<Void>
    }
    
    struct Output {
        var fetchOtherCard: Driver<Void>
        var otherCard: Driver<[Card]>
        var detailCard: Driver<CardDetailViewObject>
        var fetchDetailActivity: Driver<Bool>
        var paginationActivity: Driver<Bool>
        var error: Driver<Error>
    }
}

class CardDetailViewModel {
    
    private var useCase: CardListUseCase!
    private let fetchDetailactivity = ActivityTracker()
    private let paginationActivity = ActivityTracker()
    private let error = ErrorTracker()
    private var currentPage = 1
    private var totalCount = 1
    private var isLoadMore = false
    
    init(useCase: CardListUseCase) {
        self.useCase = useCase
    }
    
    func transform(input: Input) -> Output {
        let cardList = BehaviorRelay<[Card]>(value: [])
        
        let fetchDetail = input.fetchDetailCard
            .flatMapLatest { cardId in
                return self.useCase
                    .fetchDetailCard(id: cardId)
                    .trackError(self.error)
                    .trackActivity(self.fetchDetailactivity)
                    .asDriverOnErrorJustComplete()
            }
        
        let otherCard = Driver.combineLatest(
            input.fetchDetailCard,
            input.fetchPagination.startWith(())
        ).filter{ _ in !self.isLoadMore && cardList.value.count < self.totalCount}
        .do{_ in self.isLoadMore = true}
        .flatMapLatest { _ in
            return self.useCase.fetchCard(
                search: "",
                currentPage: String(self.currentPage)
            ).do(onCompleted: {
                self.isLoadMore = false
            }).trackError(self.error)
            .trackActivity(self.paginationActivity)
            .asDriverOnErrorJustComplete()
        }.do { data in
            var cards = cardList.value
            cards.append(contentsOf: data.cards)
            cardList.accept(cards)
            
            self.currentPage += 1
            self.totalCount = data.totalCount
        }
        
        return Output(
            fetchOtherCard: otherCard.mapToVoid(),
            otherCard: cardList.asDriver().skip(1),
            detailCard: fetchDetail,
            fetchDetailActivity: fetchDetailactivity.asDriver(),
            paginationActivity: paginationActivity.asDriver(),
            error: self.error.asDriver()
        )
    }
}

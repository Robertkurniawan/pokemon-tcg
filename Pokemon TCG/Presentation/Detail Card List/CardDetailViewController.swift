//
//  CardDetailViewcontroller.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 08/06/23.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import SnapKit
import Kingfisher
import SkeletonView

class CardDetailViewcontroller: UIViewController {
    
    //MARK: - UI Component
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        scroll.isSkeletonable = true
        return scroll
    }()
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.isSkeletonable = true
        return view
    }()
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = .clear
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 0
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 20, right: 10)
        stackView.isSkeletonable = true
        return stackView
    }()
    private lazy var imageCard: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleToFill
        image.backgroundColor = .clear
        image.isUserInteractionEnabled = false
        image.layer.cornerRadius = 8
        image.clipsToBounds = true
        image.isSkeletonable = true
        image.skeletonCornerRadius = 8
        image.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(previewImgTapped(_:))
            )
        )
        return image
    }()
    private lazy var cardNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        label.textColor = .white
        label.isSkeletonable = true
        label.linesCornerRadius = 4
        label.lastLineFillPercent = 20
        return label
    }()
    private lazy var hpLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.textColor = .white
        label.isSkeletonable = true
        label.linesCornerRadius = 4
        label.lastLineFillPercent = 30
        return label
    }()
    private lazy var cardTypeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.textColor = .white
        label.isSkeletonable = true
        label.linesCornerRadius = 4
        label.lastLineFillPercent = 30
        return label
    }()
    private lazy var flavor: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        label.text = "Falvor :"
        label.textColor = .white
        label.isSkeletonable = true
        label.linesCornerRadius = 4
        label.lastLineFillPercent = 75
        return label
    }()
    private lazy var flavorDescLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.italicSystemFont(ofSize: 14)
        label.textColor = .white
        label.numberOfLines = 5
        label.isSkeletonable = true
        label.linesCornerRadius = 4
        label.lastLineFillPercent = 80
        label.skeletonLineSpacing = 5
        return label
    }()
    private lazy var otherCardLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        label.text = "Other Cards"
        label.textColor = .white
        label.isSkeletonable = true
        label.linesCornerRadius = 4
        label.lastLineFillPercent = 75
        return label
    }()
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 5
        layout.scrollDirection = .horizontal
        layout.estimatedItemSize = .zero
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.clipsToBounds = false
        collectionView.registerCell(CardCell.self)
        collectionView.registerCell(LoadingFooterView.self)
        collectionView.isSkeletonable = true
        return collectionView
    }()
    private lazy var indicator: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.color = .white
        return activityIndicatorView
    }()
    private lazy var msgView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    private lazy var titleMsgLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        return label
    }()
    private lazy var previewImgView: UIView = {
        let view = UIView()
        view.backgroundColor = Color.bgPreviewImage.color
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(previewImgTapped(_:))
            )
        )
        return view
    }()
    private lazy var previewImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleToFill
        image.backgroundColor = .clear
        return image
    }()
    
    
    //MARK: - Variable
    private var vm: CardDetailViewModel?
    private let bag = DisposeBag()
    private var dataSource: CardListDataSource?
    private let gradient = SkeletonGradient(
        baseColor: Color.shimmerSecondColor.color,
        secondaryColor: Color.shimmerBaseColor.color
    )
    private let animation = SkeletonAnimationBuilder().makeSlidingAnimation(
        withDirection: .leftRight
    )
    
    var cardObj: Card?

    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        bindViewModel()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
        cardNameLabel.layoutIfNeeded()
        hpLabel.layoutIfNeeded()
        cardTypeLabel.layoutIfNeeded()
    }
    
    
    //MARK: - Function
    private func setUI() {
        self.navigationController?.view.tintColor = .white
        let backButton = UIBarButtonItem()
        backButton.tintColor = .white
        backButton.title = cardObj?.name ?? ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.view.backgroundColor = Color.baseBgColor.color
        view.isSkeletonable = true
        imageCard.kf.setImage(
            with: URL(string: cardObj?.largeImage ?? ""),
            options: [.cacheOriginalImage],
            completionHandler: { result in
                switch result {
                case .success:
                    self.imageCard.isUserInteractionEnabled = true
                    self.previewImage.image = self.imageCard.image
                case .failure:
                    break
                }
            }
        )
        
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { make in
            let safeArea = self.view.safeAreaLayoutGuide
            make.top.equalTo(safeArea.snp.top)
            make.leading.equalTo(safeArea.snp.leading)
            make.trailing.equalTo(safeArea.snp.trailing)
            make.bottom.equalTo(safeArea.snp.bottom)
        }
        
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.top.equalTo(scrollView)
            make.leading.equalTo(scrollView)
            make.trailing.equalTo(scrollView)
            make.bottom.equalTo(scrollView)
            make.width.equalTo(scrollView).multipliedBy(1.0)
            make.height.equalTo(scrollView).multipliedBy(1.0).priority(250)
        }
        
        contentView.addSubview(contentStackView)
        contentStackView.snp.makeConstraints { make in
            make.top.equalTo(contentView.snp.top)
            make.leading.equalTo(contentView.snp.leading)
            make.trailing.equalTo(contentView.snp.trailing)
            make.bottom.equalTo(contentView.snp.bottom)
        }
        
        let tempImageView = UIView()
        tempImageView.backgroundColor = .clear
        tempImageView.addSubview(imageCard)
        tempImageView.isSkeletonable = true
        imageCard.snp.makeConstraints { make in
            make.top.equalTo(tempImageView.snp.top)
            make.bottom.equalTo(tempImageView.snp.bottom)
            make.centerX.equalTo(tempImageView.snp.centerX)
            make.height.equalTo(300)
            make.width.equalTo(200)
        }
        
        collectionView.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.main.bounds.height * 0.35)
        }
        
        contentStackView.addArrangedSubview(tempImageView)
        contentStackView.addArrangedSubview(cardNameLabel)
        contentStackView.addArrangedSubview(hpLabel)
        contentStackView.addArrangedSubview(cardTypeLabel)
        contentStackView.addArrangedSubview(flavor)
        contentStackView.addArrangedSubview(flavorDescLabel)
        contentStackView.addArrangedSubview(otherCardLabel)
        contentStackView.addArrangedSubview(collectionView)
        
        contentStackView.setCustomSpacing(10, after: tempImageView)
        contentStackView.setCustomSpacing(10, after: cardTypeLabel)
        contentStackView.setCustomSpacing(10, after: flavorDescLabel)
        contentStackView.setCustomSpacing(10, after: otherCardLabel)
        
        view.addSubview(indicator)
        indicator.snp.makeConstraints { make in
            make.center.equalTo(scrollView)
        }
        
        view.addSubview(msgView)
        msgView.isHidden = true
        msgView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.equalToSuperview().inset(30)
            make.trailing.equalToSuperview().inset(30)
        }
        
        msgView.addSubview(titleMsgLabel)
        msgView.addSubview(messageLabel)
        titleMsgLabel.snp.makeConstraints { make in
            make.top.equalTo(msgView.snp.top)
            make.centerX.equalTo(msgView)
        }
        messageLabel.snp.makeConstraints { make in
            make.top.equalTo(titleMsgLabel.snp.bottom).offset(12)
            make.bottom.equalTo(msgView.snp.bottom)
            make.leading.equalTo(msgView.snp.leading)
            make.trailing.equalTo(msgView.snp.trailing)
        }
        
        view.addSubview(previewImgView)
        previewImgView.isHidden = true
        previewImgView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
            make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
            make.bottom.equalToSuperview()
        }
        
        previewImgView.addSubview(previewImage)
        previewImage.snp.makeConstraints { make in
            make.height.equalToSuperview().multipliedBy(0.6)
            make.width.equalToSuperview().multipliedBy(0.75)
            make.center.equalTo(previewImgView)
        }
        
        dataSource = CardListDataSource(collectionView: collectionView)
        collectionView.dataSource = dataSource
        collectionView.delegate = dataSource
    }
    
    private func bindViewModel() {
        vm = CardDetailViewModel(
            useCase: CardListUseCaseImpl(
                repository: CardListRepositoryImpl()
            )
        )
        
        let scrollLoadMore = collectionView.rx.didScroll.filter{[unowned self] _ in
            let position = self.collectionView.contentOffset.x
            let contentHeight = self.collectionView.contentSize.width
            let frameHeight = self.collectionView.frame.width
            return position > abs(contentHeight - frameHeight)
        }
        
        let input = CardDetailViewModel.Input(
            fetchDetailCard: .just(cardObj?.id ?? ""),
            fetchPagination: scrollLoadMore.asDriverOnErrorJustComplete()
        )
        
        guard let output = vm?.transform(input: input) else {return}
        
        output.fetchOtherCard.drive().disposed(by: bag)
        
        output.detailCard.delay(.milliseconds(50)).drive(onNext: { [unowned self] detailCard in
            cardNameLabel.text = detailCard.cardName
            hpLabel.text = detailCard.cardTypes.joined(separator: ", ") + " (HP \(detailCard.cardHp))"
            cardTypeLabel.text = detailCard.superType + " - " + detailCard.subType.joined(separator: ", ")
            flavorDescLabel.text = detailCard.cardFlavor
        }).disposed(by: bag)
        
        output.otherCard.drive{[unowned self] data in
            dataSource?.data = data
        }.disposed(by: bag)
        
        output.fetchDetailActivity.drive{[unowned self] isLoading in
            scrollView.isHidden = false
            msgView.isHidden = true
            
            isLoading
            ? view.showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation)
            : view.hideSkeleton()
        }.disposed(by: bag)
        
        output.paginationActivity.drive{[unowned self] isLoading in
            dataSource?.showIndicator = isLoading
        }.disposed(by: bag)
        
        output.error.delay(.milliseconds(10)).drive {[unowned self] error in
            guard let error = error as? BaseError else {return}
            msgView.isHidden = false
            scrollView.isHidden = true
            titleMsgLabel.text = error.errorMessage.getTitle()
            messageLabel.text = error.errorMessage.getDesc()
        }.disposed(by: bag)
    }
    
    
    //MARK: - Objc Function
    @objc func previewImgTapped(_ gesture: UITapGestureRecognizer) {
        switch gesture.view {
        case previewImgView:
            previewImgView.isHidden = true
        case imageCard:
            previewImgView.isHidden = false
        default: break
        }
    }
}

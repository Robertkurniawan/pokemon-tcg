//
//  CardCell.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import UIKit
import SnapKit
import Kingfisher
import SkeletonView

class CardCell: UICollectionViewCell {
    
    lazy var cardImage: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.isSkeletonable = true
        imageView.skeletonCornerRadius = 8
        return imageView
    }()
    
    private var model: Card?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(cardImage)
        contentView.isSkeletonable = true
        contentView.superview?.isSkeletonable = true
        cardImage.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(model: Card) {
        self.model = model
        cardImage.layer.cornerRadius = 8
        cardImage.backgroundColor = Color.navigationColor.color
        if let model = self.model {
            cardImage.kf.setImage(
                with: URL(string: model.smallImage),
                options: [
                    .cacheOriginalImage
                ]
            )
        }
    }
}

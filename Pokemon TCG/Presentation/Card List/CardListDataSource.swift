//
//  CardListDataSource.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 07/06/23.
//

import Foundation
import UIKit
import SkeletonView

class CardListDataSource: NSObject{
    
    var collectionView: UICollectionView!
    var showIndicator = false {
        didSet{
            collectionView.reloadData()
        }
    }
    
    var data: [Card] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    var loggedCards = Set<String>()
    
    private var flowLayout: UICollectionViewFlowLayout?
    private let cellHeight = UIScreen.main.bounds.height * 0.35
    private let visibilityThreshold: CGFloat = 0.75
    
    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
    }
}

extension CardListDataSource: UICollectionViewDataSource {
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int) -> Int
    {
        if flowLayout?.scrollDirection == .horizontal, showIndicator {
            return data.count + 1
        }
        return data.count
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if flowLayout?.scrollDirection == .horizontal,
            showIndicator,
            indexPath.item == data.count
        {
            guard let loadingCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "LoadingFooterView",
                for: indexPath
            ) as? LoadingFooterView else {return LoadingFooterView()}
            
            loadingCell.startLoading()
            return loadingCell
        }
        
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "CardCell",
            for: indexPath
        ) as? CardCell else {return CardCell()}
        
        cell.configure(model: data[indexPath.item])
        return cell
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        at indexPath: IndexPath) -> UICollectionReusableView
    {
        if kind == UICollectionView.elementKindSectionFooter,
           flowLayout?.scrollDirection == .vertical {
            guard let aFooterView = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: "LoadingFooterView",
                for: indexPath) as? LoadingFooterView
            else {return LoadingFooterView()}
            aFooterView.backgroundColor = UIColor.clear
            showIndicator ? aFooterView.startLoading() : aFooterView.stopLoading()
            return aFooterView
        }
        return UICollectionReusableView()
    }
}

extension CardListDataSource: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize
    {   
        return CGSize(
            width: (UIScreen.main.bounds.width / 2) - 15,
            height: cellHeight
        )
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForFooterInSection section: Int
    ) -> CGSize {
        if flowLayout?.scrollDirection == .vertical {
            let height = showIndicator ? CGFloat(50) : CGFloat(0)
            return CGSize(
                width: UIScreen.main.bounds.width,
                height: height
            )
        }else {
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // Get the card ID for the displayed cell
        guard let cardId = data[safe: indexPath.row]?.id else {return}
        
        // Check if the card ID is already logged
        if !loggedCards.contains(cardId) {
            // Get the cell's frame relative to the collection view's visible bounds
            let cellFrame = collectionView.convert(cell.frame, to: collectionView.superview)
            
            // Calculate the visible portion of the cell
            let visibleHeight = min(collectionView.bounds.height, cellFrame.maxY) - max(0, cellFrame.minY)
            
            // Check if the visible portion meets the visibility threshold
            if visibleHeight >= visibilityThreshold * cellHeight {
                // Log the card ID
                print("log - \(cardId)")
                
                // Add the card ID to the logged cards set
                loggedCards.insert(cardId)
            }
        }
    }
}

extension CardListDataSource: SkeletonCollectionViewDataSource {
    
    func collectionSkeletonView(
        _ skeletonView: UICollectionView,
        numberOfItemsInSection section: Int) -> Int
    {
        return 10
    }
    
    func collectionSkeletonView(
        _ skeletonView: UICollectionView,
        cellIdentifierForItemAt indexPath: IndexPath
    ) -> ReusableCellIdentifier {
        return "CardCell"
    }
}

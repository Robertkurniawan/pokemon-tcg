//
//  CardListViewModel.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import RxSwift
import RxCocoa


extension CardListViewModel {
    struct Input {
        var searchCard: Driver<String>
        var loadCard: Driver<Void>
        var cardPagination: Driver<Void>
        var itemSelected: Driver<IndexPath>
    }
    
    struct Output {
        var fecthPokemonCard: Driver<Void>
        var loadMorePokemon: Driver<Void>
        var getCardList: Driver<[Card]>
        var selectedItem: Driver<Card>
        var activity: Driver<(Bool, ActivityState)>
        var error: Driver<Error>
    }
    
    enum ActivityState {
        case fetchCard
        case loadMore
    }
}

class CardListViewModel{
    
    private let activity = ActivityTracker()
    private let error = ErrorTracker()
    private var currentPage = 1
    private var totalCount = 0
    private let useCase: CardListUseCase!
    private var isLoadMore = false
    private var isHaveMoreData = true
    private var activtyState: ActivityState = .fetchCard
    
    init(useCase: CardListUseCase) {
        self.useCase = useCase
    }
    
    func transform(input: Input) -> Output{
        
        let cardList = BehaviorRelay<[Card]>(value: [])
        
        let fetchCard = Driver.combineLatest(
            input.loadCard,
            input.searchCard
        ).do { _ in self.currentPage = 1 ; self.activtyState = .fetchCard}
        .flatMapLatest { _, search in
            return self.useCase.fetchCard(
                search: search,
                currentPage: String(self.currentPage)
            ).trackActivity(self.activity)
            .trackError(self.error)
            .asDriverOnErrorJustComplete()
        }.do { data in
            self.totalCount = data.totalCount
            self.currentPage += 1
            cardList.accept(data.cards)
        }
        
        let loadMore = input.cardPagination
            .withLatestFrom(input.searchCard)
            .filter{ _ in !self.isLoadMore && cardList.value.count < self.totalCount}
            .do{_ in self.isLoadMore = true ; self.activtyState = .loadMore}
            .flatMapLatest { search in
                return self.useCase.fetchCard(
                    search: search,
                    currentPage: String(self.currentPage)
                ).do(onCompleted: {
                    self.isLoadMore = false
                })
                .trackActivity(self.activity)
                .trackError(self.error)
                .asDriverOnErrorJustComplete()
            }.do { data in
                self.totalCount = data.totalCount
                self.currentPage += 1
                var tempData = cardList.value
                tempData.append(contentsOf: data.cards)
                cardList.accept(tempData)
            }
        
        let selectItem = input.itemSelected
            .map { index in
                return cardList.value[index.item]
            }
        
        let activityMap = activity.map {($0, self.activtyState)}
        
        return Output(
            fecthPokemonCard: fetchCard.mapToVoid(),
            loadMorePokemon: loadMore.mapToVoid(),
            getCardList: cardList.asDriver(),
            selectedItem: selectItem,
            activity: activityMap.asDriver(),
            error: error.asDriver()
        )
    }
}

//
//  CardListViewController.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift
import SkeletonView

class CardListViewController: UIViewController {
    
    //MARK: - UI Component
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 5
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.registerCell(CardCell.self)
        collectionView.registerFooter(LoadingFooterView.self)
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
        collectionView.isSkeletonable = true
        return collectionView
    }()
    private lazy var searchBar: UISearchBar = {
        let srcBar = UISearchBar(
            frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 100)
        )
        srcBar.placeholder = "Search Pokemon Card"
        let searchField = srcBar.value(forKey: "searchField") as? UITextField
        searchField?.textColor = .white
        return srcBar
    }()
    private lazy var indicator: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.color = .white
        return activityIndicatorView
    }()
    private lazy var msgView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    private lazy var titleMsgLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        return label
    }()
    
    
    //MARK: - Variable
    private var vm: CardListViewModel?
    private let bag = DisposeBag()
    private var loadPagination = false
    private var dataSource: CardListDataSource?
    private let gradient = SkeletonGradient(
        baseColor: Color.shimmerSecondColor.color,
        secondaryColor: Color.shimmerBaseColor.color
    )
    private let animation = SkeletonAnimationBuilder().makeSlidingAnimation(
        withDirection: .leftRight
    )
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        bindViewModel()
    }
    
    
    //MARK: - Function
    private func setupUI() {
        view.backgroundColor = Color.baseBgColor.color
        view.isSkeletonable = true

        let leftNavBarButton = UIBarButtonItem(customView: searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        self.navigationController?.navigationBar.barTintColor = Color.navigationColor.color
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(10)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        view.addSubview(indicator)
        indicator.snp.makeConstraints { make in
            make.center.equalTo(collectionView)
        }
        
        view.addSubview(msgView)
        msgView.isHidden = true
        msgView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.equalToSuperview().inset(30)
            make.trailing.equalToSuperview().inset(30)
        }
        
        msgView.addSubview(titleMsgLabel)
        msgView.addSubview(messageLabel)
        titleMsgLabel.snp.makeConstraints { make in
            make.top.equalTo(msgView.snp.top)
            make.centerX.equalTo(msgView)
        }
        messageLabel.snp.makeConstraints { make in
            make.top.equalTo(titleMsgLabel.snp.bottom).offset(12)
            make.bottom.equalTo(msgView.snp.bottom)
            make.leading.equalTo(msgView.snp.leading)
            make.trailing.equalTo(msgView.snp.trailing)
        }
        
        dataSource = CardListDataSource(collectionView: collectionView)
        collectionView.dataSource = dataSource
        collectionView.delegate = dataSource
    }
    
    private func bindViewModel(){
        
        vm = CardListViewModel(
            useCase: CardListUseCaseImpl(
                repository: CardListRepositoryImpl()
            )
        )
        
        let scroll = collectionView.rx.didScroll.filter{[unowned self] _ in
            let position = collectionView.contentOffset.y
            let contentHeight = collectionView.contentSize.height
            let frameHeight = collectionView.frame.height
            return position > abs(contentHeight - frameHeight)
        }

        let input = CardListViewModel.Input(
            searchCard: searchBar.rx.text.changed.unwrap().asDriverOnErrorJustComplete().startWith("")
                .debounce(.milliseconds(600))
                .do{_ in self.dataSource?.loggedCards.removeAll()},
            loadCard: Driver.just(()),
            cardPagination: scroll.asDriverOnErrorJustComplete(),
            itemSelected: collectionView.rx.itemSelected.asDriver()
        )
        
        guard let output = vm?.transform(input: input) else {return}
        
        output.fecthPokemonCard.drive().disposed(by: bag)
        output.loadMorePokemon.drive().disposed(by: bag)

        output.getCardList.drive{ data in
            self.collectionView.isHidden = false
            self.dataSource?.data = data
        }.disposed(by: bag)
        
        output.selectedItem.drive{ card in
            let vc = CardDetailViewcontroller()
            vc.cardObj = card
            self.navigationController?.pushViewController(vc, animated: true)
        }.disposed(by: bag)
        
        output.activity.drive(onNext: {[unowned self] isLoading, state in
            switch state {
            case .fetchCard:
                collectionView.isHidden = false
                msgView.isHidden = true
                isLoading
                ? view.showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation)
                : view.hideSkeleton()
            case .loadMore:
                self.dataSource?.showIndicator = isLoading
            }
        }).disposed(by: bag)
        
        output.error.delay(.milliseconds(10)).drive {[unowned self] error in
            guard let error = error as? BaseError else {return}
            msgView.isHidden = false
            collectionView.isHidden = true
            titleMsgLabel.text = error.errorMessage.getTitle()
            messageLabel.text = error.errorMessage.getDesc()
        }.disposed(by: bag)
    }
}

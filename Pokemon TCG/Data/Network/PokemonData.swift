//
//  PokemonData.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import SwiftyJSON

struct Pokemon {
    let id: String
    let name: String
    let supertype: String
    let subtypes: [String]
    let hp: String
    let types: [String]
    let evolvesTo: [String]
    let rules: [String]
    let attacks: [Attack]
    let weaknesses: [Weakness]
    let retreatCost: [String]
    let convertedRetreatCost: Int
    let set: SetInfo
    let number: String
    let artist: String
    let rarity: String
    let nationalPokedexNumbers: [Int]
    let legalities: Legalities
    let images: ImageUrls
    let tcgplayer: TCGPlayerInfo
    let flavorTxt: String
    
    init(json: JSON) {
        id = json["id"].stringValue
        name = json["name"].stringValue
        supertype = json["supertype"].stringValue
        subtypes = json["subtypes"].arrayValue.map { $0.stringValue }
        hp = json["hp"].stringValue
        types = json["types"].arrayValue.map { $0.stringValue }
        evolvesTo = json["evolvesTo"].arrayValue.map { $0.stringValue }
        rules = json["rules"].arrayValue.map { $0.stringValue }
        attacks = json["attacks"].arrayValue.map { Attack(json: $0) }
        weaknesses = json["weaknesses"].arrayValue.map { Weakness(json: $0) }
        retreatCost = json["retreatCost"].arrayValue.map { $0.stringValue }
        convertedRetreatCost = json["convertedRetreatCost"].intValue
        set = SetInfo(json: json["set"])
        number = json["number"].stringValue
        artist = json["artist"].stringValue
        rarity = json["rarity"].stringValue
        nationalPokedexNumbers = json["nationalPokedexNumbers"].arrayValue.map { $0.intValue }
        legalities = Legalities(json: json["legalities"])
        images = ImageUrls(json: json["images"])
        tcgplayer = TCGPlayerInfo(json: json["tcgplayer"])
        flavorTxt = json["flavorText"].stringValue
    }
}

struct Attack {
    let name: String
    let cost: [String]
    let convertedEnergyCost: Int
    let damage: String
    let text: String
    
    init(json: JSON) {
        name = json["name"].stringValue
        cost = json["cost"].arrayValue.map { $0.stringValue }
        convertedEnergyCost = json["convertedEnergyCost"].intValue
        damage = json["damage"].stringValue
        text = json["text"].stringValue
    }
}

struct Weakness {
    let type: String
    let value: String
    
    init(json: JSON) {
        type = json["type"].stringValue
        value = json["value"].stringValue
    }
}

struct SetInfo {
    let id: String
    let name: String
    let series: String
    let printedTotal: Int
    let total: Int
    let legalities: Legalities
    let ptcgoCode: String
    let releaseDate: String
    let updatedAt: String
    let images: ImageSet
    
    init(json: JSON) {
        id = json["id"].stringValue
        name = json["name"].stringValue
        series = json["series"].stringValue
        printedTotal = json["printedTotal"].intValue
        total = json["total"].intValue
        legalities = Legalities(json: json["legalities"])
        ptcgoCode = json["ptcgoCode"].stringValue
        releaseDate = json["releaseDate"].stringValue
        updatedAt = json["updatedAt"].stringValue
        images = ImageSet(json: json["images"])
    }
}

struct ImageSet {
    let symbol: String
    let logo: String
    
    init(json: JSON) {
        self.symbol = json["symbol"].stringValue
        self.logo = json["logo"].stringValue
    }
}

struct Legalities {
    let unlimited: String
    let expanded: String
    
    init(json: JSON) {
        unlimited = json["unlimited"].stringValue
        expanded = json["expanded"].stringValue
    }
}

struct ImageUrls {
    let small: String
    let large: String
    
    init(json: JSON) {
        small = json["small"].stringValue
        large = json["large"].stringValue
    }
}

struct TCGPlayerInfo {
    let url: String
    let updatedAt: String
    let prices: Prices
    
    init(json: JSON) {
        url = json["url"].stringValue
        updatedAt = json["updatedAt"].stringValue
        prices = Prices(json: json["prices"])
    }
}

struct Prices {
    let holofoil: PriceInfo
    
    init(json: JSON) {
        holofoil = PriceInfo(json: json["holofoil"])
    }
}

struct PriceInfo {
    let low: Double
    let mid: Double
    let high: Double
    let market: Double
    let directLow: Double
    
    init(json: JSON) {
        low = json["low"].doubleValue
        mid = json["mid"].doubleValue
        high = json["high"].doubleValue
        market = json["market"].doubleValue
        directLow = json["directLow"].doubleValue
    }
}

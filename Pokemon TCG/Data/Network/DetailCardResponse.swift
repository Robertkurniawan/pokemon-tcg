//
//  DetailCardListResponse.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import SwiftyJSON

struct DetailCardResponse: BaseResponse {
    
    let pokemonData: Pokemon
    
    init(json: JSON) {
        pokemonData = Pokemon(json: json["data"])
    }
}

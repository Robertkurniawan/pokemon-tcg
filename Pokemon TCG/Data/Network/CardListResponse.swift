//
//  CardListResponse.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import SwiftyJSON


struct CardListResponse: BaseResponse {
    let pokemonData: [Pokemon]
    let page: Int
    let pageSize: Int
    let count: Int
    let totalCount: Int
    
    init(json: JSON) {
        pokemonData = json["data"].arrayValue.map{Pokemon(json: $0)}
        page = json["page"].intValue
        pageSize = json["pageSize"].intValue
        count = json["count"].intValue
        totalCount = json["totalCount"].intValue
    }
}




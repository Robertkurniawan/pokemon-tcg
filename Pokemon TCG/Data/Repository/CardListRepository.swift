//
//  CardListRepository.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import RxSwift
import Alamofire

class CardListRepositoryImpl: CardListRepository {
    
    weak var cardListRequest: DataRequest?
    
    func fetchCardList(request: BaseRequest) -> Observable<CardListViewObject> {
        
        cardListRequest?.cancel()
        
        return API.request(
            input: request,
            dataRequest: { dataRequest in
                self.cardListRequest = dataRequest
            }
        ).map { response -> CardListResponse in
            return response
        }.map { dataResponse in
            return CardListViewObject(
                totalCount: dataResponse.totalCount,
                cards: dataResponse.pokemonData.map{
                    Card(id: $0.id,
                         name: $0.name,
                         largeImage: $0.images.large,
                         smallImage: $0.images.small
                    )
                }
            )
        }
    }
    
    func fetchCardDetail(request: BaseRequest) -> RxSwift.Observable<CardDetailViewObject> {
        return API.request(input: request)
            .map { response -> DetailCardResponse in
                return response
            }.map { dataResponse in
                return CardDetailViewObject(
                    cardName: dataResponse.pokemonData.name,
                    cardImage: dataResponse.pokemonData.images.large,
                    cardHp: dataResponse.pokemonData.hp,
                    cardTypes: dataResponse.pokemonData.types,
                    cardFlavor: dataResponse.pokemonData.flavorTxt,
                    superType: dataResponse.pokemonData.supertype,
                    subType: dataResponse.pokemonData.subtypes
                )
            }
    }
}

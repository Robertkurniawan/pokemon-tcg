//
//  CardListUseCase.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 07/06/23.
//

import Foundation
import RxSwift
import Alamofire

final class CardListUseCaseImpl: CardListUseCase {
    
    private let repository: CardListRepository
    private var cards: [Card] = []
    
    init(repository: CardListRepository) {
        self.repository = repository
    }
    
    func fetchCard(
        search: String,
        currentPage: String
    ) -> Observable<CardListViewObject> {
        let param = search == "" ? [
            "page" : currentPage,
            "pageSize" : 10,
        ] : [
            "page" : currentPage,
            "pageSize" : 10,
            "q" : "name:" + search
        ]
        
        let request = BaseRequest(
            url: URLs.cardsUrl,
            requestType: .get,
            encoding: URLEncoding.default,
            body: param
        )
        
        return repository.fetchCardList(request: request)
            .delaySubscription(
                .microseconds(300),
                scheduler: MainScheduler.instance
            ).subscribe(
                on: ConcurrentDispatchQueueScheduler(qos: .background)
            ).observe(on: MainScheduler.instance)
            .map { cardObj in
                if cardObj.cards.isEmpty {
                    throw BaseError.customError(
                        code: "999",
                        title: "Opps...!",
                        desc: "Pokemon card is not found!"
                    )
                }
                return cardObj
            }
    }
    
    func fetchDetailCard(id: String) -> Observable<CardDetailViewObject> {
        let requst = BaseRequest(url: URLs.cardsUrl + "/\(id)")
        
        return repository.fetchCardDetail(request: requst)
            .delaySubscription(
                .microseconds(300),
                scheduler: MainScheduler.instance
            ).subscribe(
                on: ConcurrentDispatchQueueScheduler(qos: .background)
            ).observe(on: MainScheduler.instance)
            .do(onNext: { card in
                card.cardFlavor = card.cardFlavor == "" ? "-" : card.cardFlavor
            })
    }    
}

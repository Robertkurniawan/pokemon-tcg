//
//  CardRepoProtocol.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import RxSwift

protocol CardListRepository {
    func fetchCardList(request: BaseRequest) -> Observable<CardListViewObject>
    func fetchCardDetail(request: BaseRequest) -> Observable<CardDetailViewObject>
}

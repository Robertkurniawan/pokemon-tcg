//
//  CardListUseCase.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 07/06/23.
//

import Foundation
import RxSwift

protocol CardListUseCase{
    func fetchCard(
        search: String,
        currentPage: String
    ) -> Observable<CardListViewObject>
    
    func fetchDetailCard(id: String) -> Observable<CardDetailViewObject>
}

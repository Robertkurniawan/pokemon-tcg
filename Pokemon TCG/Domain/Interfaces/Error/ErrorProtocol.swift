//
//  ErrorProtocol.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation

protocol ErrorProtocol{
    func getErrCode() -> String
    func getTitle() -> String
    func getDesc() -> String
}

//
//  CardDetailViewObject.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 08/06/23.
//

import Foundation

class CardDetailViewObject {
    var cardName: String
    var cardImage: String
    var cardHp: String
    var cardTypes: [String]
    var cardFlavor: String
    var superType: String
    var subType: [String]
    
    init(cardName: String, cardImage: String, cardHp: String, cardTypes: [String], cardFlavor: String, superType: String, subType: [String]) {
        self.cardName = cardName
        self.cardImage = cardImage
        self.cardHp = cardHp
        self.cardTypes = cardTypes
        self.cardFlavor = cardFlavor
        self.superType = superType
        self.subType = subType
    }
}

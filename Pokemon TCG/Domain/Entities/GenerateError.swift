//
//  GenerateError.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation

struct GenerateError : ErrorProtocol, Error {
    var errCode = ""
    var title = ""
    var desc = ""
    
    init(errCode: String = "", title: String, desc: String) {
        self.errCode = errCode
        self.title = title
        self.desc = desc
    }
    
    func getErrCode() -> String {
        return errCode
    }
    
    func getTitle() -> String {
        return title == "" ? "Failed" : title
    }
    
    func getDesc() -> String {
        return desc
    }
}

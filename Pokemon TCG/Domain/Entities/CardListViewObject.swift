//
//  CardListViewObject.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 07/06/23.
//

import Foundation

class CardListViewObject {
    var totalCount: Int
    var cards: [Card]
    
    init(totalCount: Int, cards: [Card]) {
        self.totalCount = totalCount
        self.cards = cards
    }
}

class Card {
    var id: String
    var name: String
    var largeImage: String
    var smallImage: String
    
    init(id: String, name: String, largeImage: String, smallImage: String) {
        self.id = id
        self.name = name
        self.largeImage = largeImage
        self.smallImage = smallImage
    }
}

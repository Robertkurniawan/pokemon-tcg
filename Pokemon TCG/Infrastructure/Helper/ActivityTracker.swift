//
//  ActivityTracker.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import RxSwift
import RxCocoa

private struct ActivityToken<E> : ObservableConvertibleType, Disposable {
    private let _source: Observable<E>
    private let _dispose: Cancelable
    
    init(source: Observable<E>, disposeAction: @escaping () -> Void) {
        _source = source
        _dispose = Disposables.create(with: disposeAction)
    }
    
    func dispose() {
        _dispose.dispose()
    }
    
    func asObservable() -> Observable<E> {
        _source
    }
}

/**
 Enables monitoring of sequence computation.
 If there is at least one sequence computation in progress, `true` will be sent.
 When all activities complete `false` will be sent.
 */
public class ActivityTracker : SharedSequenceConvertibleType {
    public typealias Element = Bool
    public typealias SharingStrategy = DriverSharingStrategy
    
    private let _lock = NSRecursiveLock()
    private let _relay = BehaviorRelay(value: 0)
    private let activityOverride = PublishSubject<Int>()
    private let _loading: SharedSequence<SharingStrategy, Bool>

    public init() {
        self._loading = Driver
            .of(
                _relay.asDriver(),
                activityOverride.asDriver(onErrorJustReturn: 0)
            )
            .merge()
            .map { counter in counter > 0 }
            .distinctUntilChanged()
    }
    
    /// Keep activity tracking alive.
    /// Use this in cases of dependent chained observables
    /// so that the "waiting" state continues until the
    /// last observable completes. Usually call this in
    /// a "do.onNext" between observables.
    public func keepAlive() {
        _lock.lock()
        activityOverride.onNext(1)
        _lock.unlock()
        
    }
    
    func trackActivityOfObservable<Source: ObservableConvertibleType>(_ source: Source) -> Observable<Source.Element> {
        return Observable.using({ () -> ActivityToken<Source.Element> in
            self.increment()
            return ActivityToken(source: source.asObservable(), disposeAction: self.decrement)
        }) { t in
            return t.asObservable()
        }
    }
    
    private func increment() {
        _lock.lock()
        _relay.accept(_relay.value + 1)
        _lock.unlock()
    }
    
    private func decrement() {
        _lock.lock()
        _relay.accept(_relay.value - 1)
        _lock.unlock()
    }
    
    public func asSharedSequence() -> SharedSequence<SharingStrategy, Element> {
        _loading
    }
}

//
//  Color.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 07/06/23.
//

import Foundation
import UIKit

enum Color {
    case baseBgColor
    case navigationColor
    case bgPreviewImage
    case shimmerBaseColor
    case shimmerSecondColor
    
    var color: UIColor {
        switch self {
        case .baseBgColor:
            return fromRGB(rgbValue: 0x161B22)
        case .navigationColor:
            return fromRGB(rgbValue: 0x1A202C)
        case .bgPreviewImage:
            return fromRGB(rgbValue: 0x000000, alpha: 0.75)
        case .shimmerBaseColor:
            return fromRGB(rgbValue: 0x2c3039)
        case .shimmerSecondColor:
            return fromRGB(rgbValue: 0x353841)
        }
    }
    
    private func fromRGB(rgbValue: Int, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(
            red: CGFloat((Float((rgbValue & 0xff0000) >> 16)) / 255.0),
            green: CGFloat((Float((rgbValue & 0x00ff00) >> 8)) / 255.0),
            blue: CGFloat((Float((rgbValue & 0x0000ff) >> 0)) / 255.0),
            alpha: alpha
        )
    }
}

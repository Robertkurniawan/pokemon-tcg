//
//  Extension+Collection.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 09/06/23.
//

import Foundation

extension Collection where Indices.Iterator.Element == Index {
    public subscript(safe index: Index) -> Iterator.Element? {
        return (startIndex <= index && index < endIndex) ? self[index] : nil
    }
}

//
//  ObservableConvertibleType.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import RxSwift

extension ObservableConvertibleType {
    func trackError(_ errorTracker: ErrorTracker) -> Observable<Element> {
        return errorTracker.trackError(from: self)
    }
    
    public func trackActivity(_ activityIndicator: ActivityTracker) -> Observable<Element> {
        activityIndicator.trackActivityOfObservable(self)
    }
}

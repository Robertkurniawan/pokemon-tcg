//
//  Extension+UICollectionView.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import UIKit

extension UICollectionView {
    func registerCell(_ cell: AnyClass) {
        let className = String(describing: cell)
        self.register(cell.self, forCellWithReuseIdentifier: className)
    }
    
    func registerFooter(_ cell: AnyClass) {
        let className = String(describing: cell)
        self.register(
            cell.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
            withReuseIdentifier: className
        )
    }
}

//
//  BaseResponse.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import SwiftyJSON

public protocol BaseResponse {
    init(json: JSON)
}

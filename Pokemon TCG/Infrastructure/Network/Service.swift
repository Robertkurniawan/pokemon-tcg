//
//  Service.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import Alamofire
import SwiftyJSON
import RxSwift


let API = APIService.share

class APIService {
    static let share = APIService()
    
    private var alamofireManager = Session()
    
    init() {
        let networkLogger = AlamofireLogger()
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 15
        configuration.timeoutIntervalForResource = 15
        alamofireManager = Session(
            configuration: configuration,
            delegate: Session.default.delegate,
            eventMonitors: [networkLogger]
        )
    }
    
    func request<T: BaseResponse>(
        input: BaseRequest,
        dataRequest: ((DataRequest?) -> Void)? = nil
    ) -> Observable<T> {
        weak var request: DataRequest?
        return Observable.create{ observer in
            
            request = self.alamofireManager.request(
                input.url,
                method: input.requestType,
                parameters: input.body,
                encoding: input.encoding ?? URLEncoding.default)
            .responseData { response in
                print("CALL API ")
                switch response.result{
                case .success(let value):
                    if let statusCode = response.response?.statusCode {
                        if statusCode == 200 {
                            if let object = Mapping<T>().map(JSONObject: value) {
                                observer.onNext(object)
                            }
                        } else {
                            if let object = Mapping<ErrorResponse>().map(JSONObject: value) {
                                observer.onError(BaseError.apiFailure(object))
                            } else {
                                observer.onError(BaseError.httpError(httpCode: statusCode))
                            }
                        }
                    } else {
                        observer.onError(BaseError.unexpectedError)
                    }
                    observer.onCompleted()
                case .failure:
                    observer.onError(BaseError.networkError)
                    observer.onCompleted()
                }
            }
            dataRequest?(request)
            return Disposables.create{
                request?.cancel()
            }
        }
    }
}

public final class Mapping<N: BaseResponse> {
    /// Maps a JSON object to an existing Mappable object if it is a JSON dictionary, or returns the passed object as is
    public func map(JSONObject: Any?) -> N? {
        
        if let theJson = JSONObject as? Data {
            let jsonObj = JSON(theJson)
            let baseResponse = N.self as BaseResponse.Type
            if let object = baseResponse.init(json: jsonObj) as? N {
                return object
            }
        }
        return nil
    }
}

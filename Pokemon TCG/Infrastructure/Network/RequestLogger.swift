//
//  RequestLogger.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import Alamofire
import SwiftyJSON

class AlamofireLogger: EventMonitor {
    //1
    let queue = DispatchQueue(label: "alamofire logger")
    //2
    func requestDidFinish(_ request: Request) {
        var message = ""
        let url = request.request?.url?.absoluteString
        let header = request.request?.headers.description
        guard let body = request.request?.httpBody else {
            return
        }
        message.append("REQUEST LOG: \n")
        if let urlPrint = url {
            message.append("URL: \n")
            message.append(urlPrint)
            message.append("\n")
        }
        if let headerPrint = header {
            message.append("Header: \n")
            message.append(headerPrint)
            message.append("\n")
        }
        
        let json = JSON(body as Any)
        var jsonMsg = json.rawString() ?? "Empty Json"
        if jsonMsg.count > 1500 {
            jsonMsg = String(jsonMsg.prefix(1000))
        }
        message.append("Request Body: \n")
        message.append(json.rawString() ?? "Empty Json")
        print(formatLog(message: message))
    }
    //3
    func request<Value>(_ request: DataRequest, didParseResponse response: DataResponse<Value, AFError>) {
        var message = ""
        guard let data = response.data else {
            return
        }

        let json = JSON(data as Any)
        message.append("RESPONSE LOG: \n")
        if let urlPrint = response.request?.url?.absoluteString {
            message.append("URL: \n")
            message.append(urlPrint)
            message.append("\n")
        }
        if let statusCode = response.response?.statusCode {
            message.append("Status Code: ")
            message.append(String(statusCode))
            message.append("\n")
        }
        message.append("Response Body: \n")
        message.append(json.rawString() ?? "Empty Json")
        print(formatLog(message: message))
    }
    
    private func formatLog(message: String)-> String{
        var data = "\n"
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss.SSS"
        let dateString = formatter.string(from: Date())
        data.append("Datetime: " + dateString + "\n")
        data.append(message)
        data.append("\n")
        data.append("====================\n")
        data.append("====================\n")
        data.append("====================\n")
        return data
    }
}

//
//  BaseError.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import SwiftyJSON

enum BaseError: Error {
    
    case networkError
    case httpError(httpCode: Int)
    case unexpectedError
    case apiFailure(ErrorResponse)
    case customError(code: String, title: String, desc: String)
    
    public var errorMessage: ErrorProtocol {
        switch self {
        case .networkError:
            return GenerateError(errCode: "999", title: "Network Error", desc: "Network error, please check your internet connection!")
        case .httpError(let code):
            return getHttpErrorMessage(httpCode: code)
        case .apiFailure(let error):
            return GenerateError(
                errCode: error.err_code,
                title: "Api Failure",
                desc: error.err_msg
            )
        case .customError(let code, let title, let desc):
            return GenerateError(
                errCode: code,
                title: title,
                desc: desc
            )
        default:
            return GenerateError(
                errCode: "0",
                title: "Server Errors",
                desc: "Something went wrong on our end."
            )
        }
    }
    
    private func getHttpErrorMessage(httpCode: Int) -> ErrorProtocol {
        let strHttpCode = String(httpCode)

        if (httpCode >= 400 && httpCode < 402) {
            // Client error
            return GenerateError(
                errCode: strHttpCode,
                title: "Bad Request",
                desc: "The request was unacceptable, often due to an incorrect query string parameter"
            )
        }
        
        if httpCode == 402 {
            return GenerateError(
                errCode: strHttpCode,
                title: "Request Failed",
                desc: "The parameters were valid but the request failed."
            )
        }
        
        if httpCode == 403 {
            return GenerateError(
                errCode: strHttpCode,
                title: "Forbidden",
                desc: "The user doesn't have permissions to perform the request."
            )
        }
        
        if httpCode == 404 {
            return GenerateError(
                errCode: strHttpCode,
                title: "Not Found",
                desc: "The requested resource doesn't exist."
            )
        }
        
        if httpCode == 429 {
            return GenerateError(
                errCode: strHttpCode,
                title: "Too Many Requests",
                desc: "The rate limit has been exceeded."
            )
        }
        
        if (httpCode >= 500 && httpCode <= 504) {
            // Server error
            return GenerateError(
                errCode: strHttpCode,
                title: "Server Errors",
                desc: "Something went wrong on our end."
            )
        }
        // Unofficial error
        return GenerateError(
            errCode: strHttpCode,
            title: "Opps...!",
            desc: "Something went wrong on our end."
        )
    }
}

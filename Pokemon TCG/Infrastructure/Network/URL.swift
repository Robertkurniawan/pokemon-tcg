//
//  URL.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation

struct URLs {
    
    private static let APIBaseURL = "https://api.pokemontcg.io/v2/"
    
    static let cardsUrl = APIBaseURL + "cards"
}

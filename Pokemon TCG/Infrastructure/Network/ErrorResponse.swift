//
//  ErrorResponse.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import SwiftyJSON

struct ErrorResponse: BaseResponse {
    
    var err_code = ""
    var err_msg = ""
    
    init(json: JSON) {
        err_code = json["code"].exists() ? json["code"].stringValue : "000"
        err_msg = json["message"].exists() ? json["message"].stringValue : "Oopss...\nSomething went wrong while fetch the API"
    }
}

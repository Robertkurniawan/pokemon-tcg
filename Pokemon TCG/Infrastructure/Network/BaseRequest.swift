//
//  BaseRequest.swift
//  Pokemon TCG
//
//  Created by Robert Kurniawan on 06/06/23.
//

import Foundation
import Alamofire

class BaseRequest: NSObject{
    var url = ""
    var requestType = Alamofire.HTTPMethod.get
    var encoding: ParameterEncoding?
    var body: [String: Any]?
    
    init(url: String) {
        super.init()
        self.url = url
    }
    
    init(url: String, requestType: Alamofire.HTTPMethod) {
        super.init()
        self.url = url
        self.requestType = requestType
    }
    
    init(url: String, requestType: Alamofire.HTTPMethod, encoding: ParameterEncoding) {
        super.init()
        self.url = url
        self.requestType = requestType
        self.encoding = encoding
    }
    
    init(url: String, requestType: Alamofire.HTTPMethod, encoding: ParameterEncoding, body: [String: Any]?) {
        super.init()
        self.url = url
        self.requestType = requestType
        self.encoding = encoding
        self.body = body
    }
}

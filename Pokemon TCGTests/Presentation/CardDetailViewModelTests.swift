//
//  CardDetailViewModelTests.swift
//  Pokemon TCGTests
//
//  Created by Robert Kurniawan on 08/06/23.
//

import Foundation
import XCTest
import RxSwift
import RxCocoa
@testable import Pokemon_TCG // Import your app's module

class CardDetailViewModelTests: XCTestCase {
    
    var viewModel: CardDetailViewModel!
    var useCaseMock: CardListUseCaseMock!
    var disposeBag: DisposeBag!
    
    override func setUp() {
        super.setUp()
        useCaseMock = CardListUseCaseMock()
        viewModel = CardDetailViewModel(useCase: useCaseMock)
        disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        viewModel = nil
        useCaseMock = nil
        disposeBag = nil
        super.tearDown()
    }
    
    func testTransform_fetchDetail() {
        // Arrange
        let fetchDetailCardTrigger = PublishSubject<String>()
        let fetchPaginationTrigger = PublishSubject<Void>()
        
        let input = CardDetailViewModel.Input(
            fetchDetailCard: fetchDetailCardTrigger.asDriver(onErrorJustReturn: ""),
            fetchPagination: fetchPaginationTrigger.asDriver(onErrorJustReturn: ())
        )
    
        var detailCard: CardDetailViewObject?
           
        let expectation = self.expectation(description: "Fetch card detail expectation")
        
        let output = viewModel.transform(input: input)
        
        output.detailCard.drive(onNext: { cardDetailViewObject in
            detailCard = cardDetailViewObject
        }).disposed(by: disposeBag)
        
        // Act
        fetchDetailCardTrigger.onNext("1234")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertNotNil(detailCard)
    }
    
    func testTransform_fetchOtherCard() {
        // Arrange
        let fetchDetailCardTrigger = PublishSubject<String>()
        let fetchPaginationTrigger = PublishSubject<Void>()
        
        let input = CardDetailViewModel.Input(
            fetchDetailCard: fetchDetailCardTrigger.asDriverOnErrorJustComplete(),
            fetchPagination: fetchPaginationTrigger.asDriverOnErrorJustComplete()
        )
        
        var otherCard: [Card] = []
        
        let expectation = self.expectation(description: "Fetch other card expectation")
        
        let output = viewModel.transform(input: input)
        output.fetchOtherCard.drive().disposed(by: disposeBag)
        output.otherCard.drive(onNext: { cards in
            otherCard.append(contentsOf: cards)
        }).disposed(by: disposeBag)
        
        // Act
        fetchDetailCardTrigger.onNext("1234")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertFalse(otherCard.isEmpty)
    }
    
    func testTransform_pagination() {
        // Arrange
        let fetchDetailCardTrigger = PublishSubject<String>()
        let fetchPaginationTrigger = PublishSubject<Void>()
        
        let input = CardDetailViewModel.Input(
            fetchDetailCard: fetchDetailCardTrigger.asDriverOnErrorJustComplete(),
            fetchPagination: fetchPaginationTrigger.asDriverOnErrorJustComplete()
        )
        
        var otherCard: [Card] = []
        
        let expectation = self.expectation(description: "Fetch pagination other card expectation")
        
        let output = viewModel.transform(input: input)
        output.fetchOtherCard.drive().disposed(by: disposeBag)
        output.otherCard.drive(onNext: { cards in
            otherCard.append(contentsOf: cards)
        }).disposed(by: disposeBag)
        
        // Act
        fetchDetailCardTrigger.onNext("1234")
        fetchPaginationTrigger.onNext(())
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertTrue(otherCard.count > 1)
    }
    
    func testTransform_fetchDetailActivity() {
        // Arrange
        let fetchDetailCardTrigger = PublishSubject<String>()
        let fetchPaginationTrigger = PublishSubject<Void>()
        
        let input = CardDetailViewModel.Input(
            fetchDetailCard: fetchDetailCardTrigger.asDriverOnErrorJustComplete(),
            fetchPagination: fetchPaginationTrigger.asDriverOnErrorJustComplete()
        )
        
        var activity: Bool?
        
        let expectation = self.expectation(description: "Fetch Detail Activity expectation")
        
        let output = viewModel.transform(input: input)
        
        output.detailCard.drive().disposed(by: disposeBag)
        
        output.fetchDetailActivity.drive(onNext: { state in
            activity = state
        }).disposed(by: disposeBag)
        
        // Act
        fetchDetailCardTrigger.onNext("")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertNotNil(activity)
    }
    
    func testTransform_paginationActivity() {
        // Arrange
        let fetchDetailCardTrigger = PublishSubject<String>()
        let fetchPaginationTrigger = PublishSubject<Void>()
        
        let input = CardDetailViewModel.Input(
            fetchDetailCard: fetchDetailCardTrigger.asDriverOnErrorJustComplete(),
            fetchPagination: fetchPaginationTrigger.asDriverOnErrorJustComplete()
        )
        
        var activity: Bool?
        
        let expectation = self.expectation(description: "Pagination Activity expectation")
        
        let output = viewModel.transform(input: input)
        
        output.detailCard.drive().disposed(by: disposeBag)
        output.fetchOtherCard.drive().disposed(by: disposeBag)
        output.otherCard.drive().disposed(by: disposeBag)
        
        output.paginationActivity.drive(onNext: { state in
            activity = state
        }).disposed(by: disposeBag)
        
        // Act
        fetchPaginationTrigger.onNext(())
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertNotNil(activity)
    }
    
    func testTransform_error() {
        // Arrange
        let fetchDetailCardTrigger = PublishSubject<String>()
        let fetchPaginationTrigger = PublishSubject<Void>()
        useCaseMock.isErrorTesting = true
        
        let input = CardDetailViewModel.Input(
            fetchDetailCard: fetchDetailCardTrigger.asDriverOnErrorJustComplete(),
            fetchPagination: fetchPaginationTrigger.asDriverOnErrorJustComplete()
        )
        
        var error: Error?
        
        let expectation = self.expectation(description: "Error expectation")
        
        let output = viewModel.transform(input: input)
        
        output.detailCard.drive().disposed(by: disposeBag)
        output.error.drive(onNext: { err in
            error = err
        }).disposed(by: disposeBag)
        
        // Act
        fetchDetailCardTrigger.onNext("1234")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertNotNil(error)
        XCTAssertTrue(error is BaseError)
    }
}

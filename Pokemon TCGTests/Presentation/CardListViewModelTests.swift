//
//  CardListViewModelTests.swift
//  Pokemon TCGTests
//
//  Created by Robert Kurniawan on 08/06/23.
//

import Foundation
import XCTest
import RxSwift
import RxCocoa
@testable import Pokemon_TCG // Import your app's module

class CardListViewModelTests: XCTestCase {
    
    var viewModel: CardListViewModel!
    var useCaseMock: CardListUseCaseMock!
    var disposeBag: DisposeBag!
    
    override func setUp() {
        super.setUp()
        useCaseMock = CardListUseCaseMock()
        viewModel = CardListViewModel(useCase: useCaseMock)
        disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        viewModel = nil
        useCaseMock = nil
        disposeBag = nil
        super.tearDown()
    }
    
    func input(
        searchCard: Driver<String> = .never(),
        loadCard: Driver<Void> = .never(),
        cardPagination: Driver<Void> = .never(),
        itemSelected: Driver<IndexPath> = .never()) -> CardListViewModel.Input
    {
        return CardListViewModel.Input(
            searchCard: searchCard,
            loadCard: loadCard,
            cardPagination: cardPagination,
            itemSelected: itemSelected
        )
    }
    
    func testTransform_fetchCard() {
        // Arrange
        let searchCardTrigger = PublishSubject<String>()
        let loadCardTrigger = PublishSubject<Void>()
        
        let input = self.input(
            searchCard: searchCardTrigger.asDriverOnErrorJustComplete().startWith(""),
            loadCard: loadCardTrigger.asDriverOnErrorJustComplete()
        )
        
        var fetchPokemonCard: Bool = false
        
        let expectation = self.expectation(description: "Fetch card expectation")
        
        let output = viewModel.transform(input: input)
        
        output.fecthPokemonCard.drive(onNext: { _ in
            fetchPokemonCard = true
        }).disposed(by: disposeBag)
        
        // Act
        loadCardTrigger.onNext(())
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertTrue(fetchPokemonCard)
    }
    
    func testTransform_getCardList() {
        // Arrange
        let searchCardTrigger = PublishSubject<String>()
        let loadCardTrigger = PublishSubject<Void>()
        let paginationTrigger = PublishSubject<Void>()
        
        let input = self.input(
            searchCard: searchCardTrigger.asDriverOnErrorJustComplete(),
            loadCard: loadCardTrigger.asDriverOnErrorJustComplete(),
            cardPagination: paginationTrigger.asDriverOnErrorJustComplete()
        )
        
        var getCardList: [Card] = []
        
        let expectation = self.expectation(description: "Get card list expectation")
        
        let output = viewModel.transform(input: input)
        
        output.fecthPokemonCard.drive().disposed(by: disposeBag)
        output.getCardList.drive(onNext: { cardList in
            getCardList = cardList
        }).disposed(by: disposeBag)
        
        // Act
        loadCardTrigger.onNext(())
        paginationTrigger.onNext(())
        searchCardTrigger.onNext("")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertFalse(getCardList.isEmpty)
    }
    
    func testTransform_searchCard() {
        // Arrange
        let searchCardTrigger = PublishSubject<String>()
        let loadCardTrigger = PublishSubject<Void>()
        let searchCard = "Pokemon"
        
        let input = self.input(
            searchCard: searchCardTrigger.asDriverOnErrorJustComplete(),
            loadCard: loadCardTrigger.asDriverOnErrorJustComplete()
        )
        
        var getCardList: [Card] = []
        
        let expectation = self.expectation(description: "Search and get card expectation")
        
        let output = viewModel.transform(input: input)
        
        output.fecthPokemonCard.drive().disposed(by: disposeBag)
        output.getCardList.drive(onNext: { cardList in
            getCardList = cardList
        }).disposed(by: disposeBag)
        
        // Act
        loadCardTrigger.onNext(())
        searchCardTrigger.onNext(searchCard)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertTrue(getCardList.first?.id == searchCard )
    }
    
    func testTransform_pagination() {
        // Arrange
        let searchCardTrigger = PublishSubject<String>()
        let loadCardTrigger = PublishSubject<Void>()
        let loadMoreTrigger = PublishSubject<Void>()
        
        
        let input = self.input(
            searchCard: searchCardTrigger.asDriverOnErrorJustComplete().startWith(""),
            loadCard: loadCardTrigger.asDriverOnErrorJustComplete(),
            cardPagination: loadMoreTrigger.asDriverOnErrorJustComplete()
        )
        
        var getCardList: [Card] = []
        
        let expectation = self.expectation(description: "Search and get card expectation")
        
        let output = viewModel.transform(input: input)
        
        output.fecthPokemonCard.drive().disposed(by: disposeBag)
        output.loadMorePokemon.drive().disposed(by: disposeBag)
        
        output.getCardList.drive(onNext: { cardList in
            getCardList = cardList
        }).disposed(by: disposeBag)
        
        // Act
        loadCardTrigger.onNext(())
        loadMoreTrigger.onNext(())
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertTrue(getCardList.count > 1)
    }
    
    func testTransform_selectedItem() {
        // Arrange
        let searchCardTrigger = PublishSubject<String>()
        let loadCardTrigger = PublishSubject<Void>()
        let itemSelectedTrigger = PublishSubject<IndexPath>()
        
        let input = self.input(
            searchCard: searchCardTrigger.asDriver(onErrorJustReturn: "").startWith(""),
            loadCard: loadCardTrigger.asDriver(onErrorJustReturn: ()).startWith(()),
            itemSelected: itemSelectedTrigger.asDriver(onErrorJustReturn: IndexPath(item: 0, section: 0))
        )
        
        var selectedItem: Card?
        
        let expectation = self.expectation(description: "Selected item expectation")
        
        let output = viewModel.transform(input: input)
        
        output.fecthPokemonCard.drive().disposed(by: disposeBag)
        output.selectedItem.drive(onNext: { card in
            selectedItem = card
        }).disposed(by: disposeBag)
        
        // Act
        itemSelectedTrigger.onNext(IndexPath(item: 0, section: 0))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertNotNil(selectedItem)
    }
    
    func testTransform_activity() {
        // Arrange
        let searchCardTrigger = PublishSubject<String>()
        let loadCardTrigger = PublishSubject<Void>()
        
        let input = self.input(
            searchCard: searchCardTrigger.asDriver(onErrorJustReturn: ""),
            loadCard: loadCardTrigger.asDriver(onErrorJustReturn: ())
        )
        
        var activity: (Bool, CardListViewModel.ActivityState)?
        
        let expectation = self.expectation(description: "Activity expectation")
        
        let output = viewModel.transform(input: input)
        
        output.fecthPokemonCard.drive().disposed(by: disposeBag)
        
        output.activity.drive(onNext: { state in
            activity = state
        }).disposed(by: disposeBag)
        
        // Act
        searchCardTrigger.onNext("")
        loadCardTrigger.onNext(())
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertNotNil(activity)
    }
    
    func testTransform_error() {
        // Arrange
        let searchCardTrigger = PublishSubject<String>()
        let loadCardTrigger = PublishSubject<Void>()
        useCaseMock.isErrorTesting = true
        
        let input = self.input(
            searchCard: searchCardTrigger.asDriver(onErrorJustReturn: "").startWith(""),
            loadCard: loadCardTrigger.asDriver(onErrorJustReturn: ())
        )
        
        var error: Error?
        
        let expectation = self.expectation(description: "Error expectation")
        
        let output = viewModel.transform(input: input)
        
        output.fecthPokemonCard.drive().disposed(by: disposeBag)
        output.error.drive(onNext: { err in
            error = err
        }).disposed(by: disposeBag)
        
        // Act
        loadCardTrigger.onError(BaseError.networkError)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Wait for the asynchronous operations to complete
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        // Assert
        XCTAssertNotNil(error)
        XCTAssertTrue(error is BaseError)
    }
}

//
//  CardUseCaseMock.swift
//  Pokemon TCGTests
//
//  Created by Robert Kurniawan on 08/06/23.
//

import Foundation
import RxSwift
@testable import Pokemon_TCG


// Mock CardListUseCase for testing
class CardListUseCaseMock: CardListUseCase {
    
    var isErrorTesting = false
    
    func fetchDetailCard(id: String) -> Observable<CardDetailViewObject> {
        let cardDetailViewObject = CardDetailViewObject(
            cardName: "Pokemon",
            cardImage: "Pokemon Image",
            cardHp: "180",
            cardTypes: ["Lightning"],
            cardFlavor: "",
            superType: "Premium",
            subType: ["Basic", "Ex"]
        )
        
        if isErrorTesting {
            return Observable.error(BaseError.networkError)
        }else {
            return Observable.just(cardDetailViewObject)
        }
    }
    
    func fetchCard(search: String, currentPage: String) -> Observable<CardListViewObject> {
        let id = search == "" ? "::" : search
        let cards = [Card(id: id, name: "Pikachu", largeImage: "pikachu image", smallImage: "pikachu image")]
        let cardListViewObject = CardListViewObject(totalCount: 2, cards: cards)
        if isErrorTesting {
            return Observable.error(BaseError.networkError)
        }else {
            return Observable.just(cardListViewObject)
        }
    }
}

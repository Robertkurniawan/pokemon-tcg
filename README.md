# Pokemon TCG



## Getting started

This is Pokemon iOS App that using Pokémon TCG API, which help us to search Pokémon cards list and details card (can enlarge the image), also have some feature like infinity scroll (pagination), other cards recommendation also link to details page.

The App have complete features and successful compiled.
    ✅ Card List + Pagination
    ✅ Detail Card + Zoom Image 
    ✅ Other Cards + Pagination 
    ✅ Searching

The project also use Reactive Programming with RxSwift and also have Unit Testing.

Xcode version : 14.1
Swift version : 5
Minimum OS    : 12.1

## 3rd Party Library

The project have several 3rd party library to support development.
1. RxSwift: RxSwift is a popular reactive programming library for Swift. It allows you to easily handle asynchronous and event-driven programming by using observables and operators. With RxSwift, you can write code in a more declarative and functional style, making it easier to manage complex asynchronous operations and handle data streams.

2. RxCocoa: RxCocoa is an accompanying library to RxSwift that provides reactive extensions for Cocoa and Cocoa Touch frameworks. It allows you to integrate reactive programming into our iOS applications seamlessly. RxCocoa provides extensions for UIKit and AppKit classes, allowing you to observe and bind UI elements to reactive data streams.

3. Alamofire: Alamofire is a widely-used networking library for handling HTTP requests in Swift. It provides an elegant and easy-to-use API for making network requests, handling response serialization, and working with authentication and other networking tasks. Alamofire simplifies network operations and improves the overall networking experience in Swift-based projects.

4. SwiftyJSON: SwiftyJSON is a lightweight library for parsing JSON data in Swift. It provides a simple and intuitive API for working with JSON data structures, making it easier to extract and manipulate data from JSON responses. SwiftyJSON eliminates the need for manual parsing code and reduces the complexity of handling JSON data in our projects.

5. SnapKit: SnapKit is a powerful autolayout library for iOS and macOS development. It provides a DSL (Domain-Specific Language) to describe autolayout constraints in a concise and readable manner. With SnapKit, you can easily define and update autolayout constraints programmatically, making UI development and adaptation to different screen sizes much simpler.

7. Kingfisher: Kingfisher is a widely-used library for downloading and caching images in Swift. It provides an efficient way to load and display images from URLs, as well as caching them for better performance. Kingfisher handles tasks such as downloading, caching, and even placeholder images, making it a convenient and reliable choice for image management in our apps.

8. IQKeyboardManagerSwift: IQKeyboardManagerSwift is a library that simplifies the management of the keyboard in iOS apps. It automatically handles keyboard appearance and dismissal, adjusts scroll views to avoid keyboard overlapping, and provides several other useful features related to keyboard management. IQKeyboardManagerSwift saves development time and ensures a better user experience when dealing with text input and the keyboard.

9. SkeletonView: SkeletonView is a library that helps you add skeleton loading animations to our UI components. It allows you to display placeholders that mimic the structure and layout of our actual content while it's being loaded. SkeletonView improves the perceived performance of our app by giving users visual feedback during loading times and enhances the overall user experience.


## MVVM Architecture

Pokemon TCG project use MVVM Architecture because:
1. Separation of concerns: MVVM promotes a clear separation between the user interface (View), the underlying data and logic (Model), and the intermediary component that connects the View and Model (ViewModel). This separation allows for better code organization, maintenance, and testing.

2. Testability: With MVVM, the business logic and data manipulation are encapsulated in the ViewModel, which is independent of the View. This separation makes it easier to write unit tests for the ViewModel since it can be tested in isolation, without the need for a user interface.

3. Maintainability: By separating the responsibilities into distinct components, MVVM helps to maintain a clean and maintainable codebase. Changes to the user interface or underlying data logic can be isolated and implemented without affecting other parts of the application.

4. Reusability: MVVM facilitates the reusability of components. ViewModels can be reused across multiple views, and multiple views can bind to the same ViewModel, promoting code reuse and reducing duplication.

5. Binding and Reactive Programming: MVVM often incorporates data binding mechanisms, where changes in the ViewModel are automatically reflected in the View and vice versa. This enables a reactive programming style, where the UI automatically updates based on changes in the data, reducing the need for manual updates and callbacks.

6. Collaboration: MVVM promotes a clear separation of concerns, making it easier for developers working in teams to collaborate. With well-defined responsibilities, different team members can work on the View, ViewModel, and Model components simultaneously without stepping on each other's toes.

Overall, MVVM provides a structured approach to iOS development that enhances code organization, maintainability, testability, and reusability. It encourages separation of concerns and promotes a reactive programming style, making it a popular choice for building iOS applications.
